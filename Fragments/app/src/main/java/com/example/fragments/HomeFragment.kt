package com.example.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        view.findViewById<BottomNavigationView>(R.id.bottomNavig).selectedItemId = R.id.home
        view.findViewById<BottomNavigationView>(R.id.bottomNavig).setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.exit -> {
                    Navigation.findNavController(view).navigate(R.id.nav_homeFrag_to_logFrag)
                }
                R.id.home -> { }
                R.id.web -> {
                    Navigation.findNavController(view).navigate(R.id.nav_homeFrag_to_webFrag)
                }
            }
            true
        }
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) = HomeFragment()
    }
}