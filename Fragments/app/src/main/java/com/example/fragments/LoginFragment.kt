package com.example.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationMenuView

class LoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_login, container, false)
        view.findViewById<BottomNavigationMenuView>(R.id.bottomNavig).isVisible = false;
        view.findViewById<TextView>(R.id.tvLogin).setOnClickListener{
            Navigation.findNavController(view).navigate(R.id.nav_logFrag_to_regFrag)
        }
        view.findViewById<TextView>(R.id.btLogin).setOnClickListener{
            Navigation.findNavController(view).navigate(R.id.nav_logFrag_to_homeFrag)
        }
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance() = LoginFragment()
    }
}