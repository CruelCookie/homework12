package com.example.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.*
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView

class WebFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_web, container, false)
        view.findViewById<BottomNavigationView>(R.id.bottomNavig).selectedItemId = R.id.web
        view.findViewById<BottomNavigationView>(R.id.bottomNavig).setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.exit -> {
                    Navigation.findNavController(view).navigate(R.id.nav_webFrag_to_logFrag)
                }
                R.id.web -> { }
                R.id.home -> {
                    Navigation.findNavController(view).navigate(R.id.nav_webFrag_to_homeFrag)
                }
            }
            true
        }

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance() = WebFragment()
    }
}